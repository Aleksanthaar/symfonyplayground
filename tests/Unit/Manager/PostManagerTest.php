<?php

namespace Tests\Unit\Manager;

use App\Manager\PostManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @coversDefaultClass \App\Manager\PostManager
 */
class PostManagerTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testConstruct()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $messageBus    = $this->createMock(MessageBusInterface::class);
        $postManager   = new PostManager($entityManager, $messageBus);

        $this->assertInstanceOf(PostManager::class, $postManager);
    }
}