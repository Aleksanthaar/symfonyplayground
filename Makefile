up:
	@python py-compose.py; \
	docker-compose up -d; \
	printf "\nSymfony runs at http://localhost:8080"; \
	printf "\nNginx runs at http://localhost:8000"; \
	printf "\nVarnish runs at http://localhost:8888"; \
	printf "\nMailcatcher runs at http://localhost:1080"; \
	printf "\nRabbitMQ Management runs at http://localhost:15672\n"

down:
	@docker-compose down --remove-orphans

reload: down up

godb:
	@mysql -h `docker inspect mariadb | jq .[0].NetworkSettings.Networks.symfonyplayground_default.IPAddress | cut -d \" -f2` -umysql_u -pmysql_p mysql_db

initldap:
	@docker exec openldap ldapadd -x -D "cn=admin,dc=example,dc=org" -w admin -H ldap://localhost -f /container/service/slapd/assets/init.ldif

dumpldap:
	@docker exec openldap ldapsearch -x -H ldap://localhost -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w admin

pingredis1:
	@redis-cli -h `docker inspect redis1 | jq .[0].NetworkSettings.Networks.symfonyplayground_default.IPAddress | cut -d \" -f2` ping

pingredis2:
	@redis-cli -h `docker inspect redis2| jq .[0].NetworkSettings.Networks.symfonyplayground_default.IPAddress | cut -d \" -f2` ping