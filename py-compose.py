import os
import yaml
import io

output_file = 'docker-compose.yaml'

def main():
    services                  = load_yaml('services.yaml')
    services                  = set(services['services'])
    docker_master             = load_yaml('docker-compose.master.yaml')
    docker_compose            = { "services": {} }
    docker_compose['version'] = docker_master['version']
    original_file_path        = os.path.join(os.path.dirname(__file__), output_file)

    if os.path.isfile(original_file_path):
        os.remove(original_file_path)

    for service in services:
        if service not in docker_master['services'].keys():
            print("Warning: tried to load undefined services: %s" % service)
            continue

        if not has_unmet_dependencies(services, service, docker_master['services'][service]):
            docker_compose['services'][service] = docker_master['services'][service]

    with io.open(output_file, 'w', encoding='utf8') as docker_compose_file:
        yaml.dump(docker_compose, docker_compose_file)

def has_unmet_dependencies(services, lookup_name, lookup_node):
    # No depends_on node = go on.
    if 'depends_on' not in lookup_node.keys():
        return False 

    unmet_dependencies = [ d for d in lookup_node['depends_on'] if d not in services ]

    if 0 == len(unmet_dependencies):
        return False

    print("Cannot load service %s because it has unmet dependencies: %s" % (lookup_name, ', '.join(unmet_dependencies)))

    return True

def load_yaml(file_name):
    path = os.path.join(os.path.dirname(__file__), file_name)

    assert os.path.isfile(path), "Error: %s not found at %s" % (file_name, path)

    with open(path, 'r') as file:
        try:
            return yaml.safe_load(file)
        except yaml.YAMLError as exc:
            print(exc)

if __name__ == '__main__':
    main()