<?php

namespace App\Message;

class NewPostMessage
{
    /**
     * @var string
    */
    protected $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
