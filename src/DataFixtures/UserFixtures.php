<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach (static::getUsers() as $userData) {
            $user = new User();

            $user->setEmail($userData['email']);
            $user->setFirstName($userData['firstName']);
            $user->setLastName($userData['lastName']);
            $user->setJobTitle($userData['jobTitle']);
            $user->setPhoto($userData['photo']);
            $user->setLocation($userData['location']);
            $user->setOfficePhone($userData['officePhone']);
            $user->setMobilePhone($userData['mobilePhone']);

            $encodedPassword = $this->passwordEncoder->encodePassword($user, $userData['password']);

            $user->setPassword($encodedPassword);
        }

        $manager->persist($user);
        $manager->flush();
    }

    public static function getUsers(): array
    {
        return [
            [
                'email'       => 'foo.bar@yopmail.com',
                'password'    => 'hello',
                'firstName'   => 'Foo',
                'lastName'    => 'Bar',
                'jobTitle'    => 'Digitalisation consultant',
                'photo'       => null,
                'location'    => 'Sprinfield',
                'officePhone' => '555-555-1234',
                'mobilePhone' => '555-555-5678',
            ]
        ];
    }
}
