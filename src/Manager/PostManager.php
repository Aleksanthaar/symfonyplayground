<?php

namespace App\Manager;

use App\Entity\Post;
use App\Message\NewPostMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var MessageBusInterface
     */
    protected $bus;

    public function __construct(
        EntityManagerInterface $entityManager,
        MessageBusInterface $bus
    ) {
        $this->entityManager = $entityManager;
        $this->bus           = $bus;
    }

    protected static function createOptionResolver(): OptionsResolver
    {
        return new OptionsResolver();
    }

    public function save(Post $post, array $options = [])
    {
        $options = $this->resolveCreatePost($options);

        $this->entityManager->persist($post);
        $this->entityManager->flush();

        if ($options['notify']) {
            $postId = (string) $post->getId();
            $mesage = new NewPostMessage($postId);

            $this->bus->dispatch($mesage);
        }
    }

    protected function resolveCreatePost(array $options = [])
    {
        $resolver = static::createOptionResolver();

        $resolver->setDefaults([
            'notify' => false,
        ]);

        $resolver->setAllowedTypes('notify', 'bool');

        return $resolver->resolve($options);
    }
}
