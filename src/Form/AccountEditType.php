<?php

namespace App\Form;

use App\Entity\User;
use App\Utils\TranslationDomainsInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;

class AccountEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'required' => true,
                'label'    => 'form.first_name',
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'label'    => 'form.last_name',
            ])
            ->add('location', TextType::class, [
                'label'     => 'form.location',
            ])
            ->add('officePhone', TextType::class, [
                'label'     => 'form.office_phone',
            ])
            ->add('mobilePhone', TextType::class, [
                'label'     => 'form.mobile_phone',
            ])
            ->add('photo', FileType::class, [
                'label'    => 'form.profile_picture',
                'required' => false,
                'mapped'   => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'form.error.file_type_image',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => TranslationDomainsInterface::USER,
            'data_class'         => User::class
        ]);
    }
}
