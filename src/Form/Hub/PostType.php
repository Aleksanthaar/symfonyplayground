<?php

namespace App\Form\Hub;

use DateTime;
use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Length;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'maxlength' => 50,
                    'placeholder' => 'Post title',
                ],
                'constraints' => [
                    new Length([
                        'max' => 50
                    ])
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'maxlength' => 1000,
                ],
                'data' => "What's on your mind?",
                'constraints' => [
                    new Length([
                        'max' => 1000
                    ])
                ]
            ])
            ->add('publish_date', TextType::class, [
                'empty_data' => (new \DateTime())->format('YYYY-mm-dd'),
                'data'       => (new \DateTime())->format('d/m/Y'),
                'attr'       => [
                    'class' => 'datepicker',
                ],
            ])
            ->add('location', TextType::class, [
                'attr' => [
                    'maxlength' => 50,
                    'placeholder' => 'Where was it?',
                ],
                'required'  => false,
                'constraints' => [
                    new Length([
                        'max' => 50
                    ])
                ]
            ])
            ->add('picture', FileType::class, [
                'required'   => false,
            ])
        ;

        $builder->get('publish_date')->addModelTransformer(new CallbackTransformer(
            function($value) { return $value; },
            function($value) {
                $dt = DateTime::createFromFormat('d/m/Y', $value);

                return $dt;
            },
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}