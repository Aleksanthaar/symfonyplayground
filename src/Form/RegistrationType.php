<?php

namespace App\Form;

use App\Utils\TranslationDomainsInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'required' => true,
                'label'    => 'form.first_name',
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'label'    => 'form.last_name',
            ])
            ->add('email', RepeatedType::class, [
                'type'           => EmailType::class,
                'required'       => true,
                'first_options'  => ['label' => 'form.email'],
                'second_options' => ['label' => 'form.email_confirm'],
                'constraints'    => [
                    new Email()
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type'           => PasswordType::class,
                'required'       => true,
                'first_options'  => ['label' => 'form.password'],
                'second_options' => ['label' => 'form.password_confirm'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => TranslationDomainsInterface::USER,
        ]);
    }
}
