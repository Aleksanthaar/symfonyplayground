<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(
        Request $request,
        UserManager $userManager
    ) {
        $registrationForm = $this->createForm(RegistrationType::class);

        $registrationForm->handleRequest($request);

        if ($registrationForm->isSubmitted() && $registrationForm->isValid()) {
            $user     = User::fromArray($registrationForm->getData());
            $session  = $request->getSession();
            $flashbag = $session->getFlashBag();

            try {
                $userManager->saveUser($user);

                $flashbag->add('success', 'Registration OK. You can now login.');
            } catch (\Exception $e) {
                $flashbag->add('danger', 'Registration Failed. Please contact your system administrator.');
            }

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/registration.html.twig', [
            'form' => $registrationForm->createView()
        ]);
    }   
}