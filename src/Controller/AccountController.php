<?php

namespace App\Controller;

use App\Form\AccountEditType;
use App\Manager\UserManager;
use App\Utils\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// Remove later
use Symfony\Component\Mercure\Jwt\StaticJwtProvider;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;


class AccountController extends AbstractController
{
    /**
     * @Route("/my_account", name="my_account")
     */
    public function myAccount(Request $request)
    {
        return $this->render('account/my_account.html.twig');
    }

    /**
     * @Route("/mail", name="mail_test")
     */
    public function mail(\Swift_Mailer $mailer)
    {
        $user = $this->getUser();

        $message = (new \Swift_Message('Hello!'))
            ->setFrom('system@playground.org')
            ->setTo($user->getEmail())
            ->setBody(
                '<h1>Hi '.$user->getFirstName().'!</h1><p>I\'m a friendly robot!</p>',
                'text/html'
            )
        ;

        $mailer->send($message);

        return $this->render('account/my_account.html.twig');
    }

    /**
     * @Route("/mercure", name="mercure_test")
     */
    public function mercure()
    {
        $jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InN1YnNjcmliZSI6WyJmb28iLCJiYXIiXSwicHVibGlzaCI6WyJmb28iXX19.LRLvirgONK13JgacQ_VbcjySbVhkSmHy3IznH3tA9PM';

        $publisher = new Publisher(
            'http://mercure/hub',
            new StaticJwtProvider($jwt)
        );

        $id = $publisher(new Update('https://example.com/books/1.jsonld', 'Hi from Symfony!', ['target1', 'target2']));

        return $this->render('account/my_account.html.twig');
    }

    /**
     * @Route("my_account/edit", name="my_account_edit")
     */
    public function editMyAccount(
        Request $request,
        UserManager $userManager,
        FileUploader $uploader
    ) {
        $user = $this->getUser();
        $form = $this->createForm(AccountEditType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPicture = $form['photo']->getData();

            $session  = $request->getSession();
            $flashBag = $session->getFlashBag();

            try {
                $uploader->uploadUserProfilePicture($user, $newPicture);

            } catch (FileException $e) {

                $flashBag->add('danger', 'Unable to upload picture.');
            }

            $flashBag->add('success', 'Profile updated.');

            $userManager->updateUser($user);

            return $this->redirectToRoute('my_account');
        } 

        return $this->render('account/my_account_edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
