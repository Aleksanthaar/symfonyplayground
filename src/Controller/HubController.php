<?php

namespace App\Controller;

use App\Form\Hub\PostType;
use App\Manager\PostManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HubController extends AbstractController
{
    /**
     * @Route("/hub", name="hub")
     */
    public function index(Request $request, PostManager $manager, Security $security)
    {
        $user    = $security->getUser();
        $newPost = $this->createForm(PostType::class);

        $newPost->handleRequest($request);

        if (!$newPost->isSubmitted()) {
            return $this->render('hub/index.html.twig', [
                'form'  => $newPost->createView(),
                'posts' => $user->getPosts(),
            ]);
        }

        $session = $request->getSession();

        if ($newPost->isValid()) {
            $post = $newPost->getData();

            $post->setUser($user);

            // This is ugly but temporary, do a proper Form type for that.
            $notify = $request->request->get('_notify', 'off');

            $manager->save($post, [
                'notify' => ($notify === 'on')
            ]);

            $session->getFlashBag()->add('success', 'Post published.');

            // Return to GET
            return $this->redirectToRoute('hub');
        }

        $session->getFlashBag()->add('error', 'Failed to publish post.');

        return $this->render('hub/index.html.twig', [
            'form'  => $newPost->createView(),
            'posts' => $user->getPosts(),
        ]);
    }
}
