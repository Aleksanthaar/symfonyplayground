<?php

namespace App\Handler;

use App\Message\NewPostMessage;
use App\Message\TestMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NewPostHandler implements MessageHandlerInterface
{
    public function __invoke(NewPostMessage $message)
    {
        // ... do some work - like sending an SMS message!
    }
}
