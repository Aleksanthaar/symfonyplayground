<?php

namespace App\Utils;

interface CacheDurationInterface
{
    public const ONE_HOUR  = 3600;
    public const ONE_DAY   = 86400;
    public const ONE_WEEK  = 604800;
    public const ONE_MONTH = 18144000; // We'll settle on one month = 30 days
    public const ONE_YEAR  = 31536000;
}
    
