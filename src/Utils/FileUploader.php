<?php

namespace App\Utils;

use App\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    public const PROFILE_PICTURES_DIR    = '/public/uploads/profiles/';
    public const PROFILE_PICTURES_DIR_DB = '/uploads/profiles/';

    /**
     * @var  string
     */
    protected $projectRootDir;

    public function __construct(string $projectRootDir)
    {
        $this->projectRootDir = $projectRootDir;
    }

    public function uploadUserProfilePicture(User $user, UploadedFile $file = null): void
    {
        if (!$file) {
            return ;
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $newFileName      = sprintf(
            '%s.%s',
            md5($user->getEmail()),
            $file->getClientOriginalExtension()
        );

        $actualLocation = sprintf('%s%s', $this->projectRootDir, static::PROFILE_PICTURES_DIR);

        // FileException will be caught externally

        $file->move($actualLocation, $newFileName);
        $storablePath = sprintf('%s%s', static::PROFILE_PICTURES_DIR_DB, $newFileName);
        $user->setPhoto($storablePath);
    }
}
