# Messenger

## Testing

To test if Symfony and RabbitMQ are able to communicate together, open two terminals. The first one will consume the messages:
```bash
    docker-compose exec php-fpm bin/console messenger:consume -vv
```

The other one will dispatch them:
```bash
    docker-compose exec php-fpm bin/console playground:messenger:test
```

After running the second command the first terminal's output should have informations about the message you just dispatched.