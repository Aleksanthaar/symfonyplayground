# SymfonyPlayground

## Purpose

Train for Symfony certification!

## How to use

```bash
    $ make up
    $ make initldap
```

To fully use the Makefile, make sure you have `jq` and `redis-tools` installed.

## Components

The components below are available for testing

- [ ] The Asset Component (need to code a dummy local CDN)
- [X] The BrowserKit Component (Nginx serving static HTML files)
- [ ] The Cache Component (need to add a Memcache container)
- [X] The Config Component
- [X] The Console Components
- [X] The CssSelector Component
- [X] The Debug Component
- [X] The DependencyInjection Component
- [X] The DomCrawler Component
- [X] The Dotenv Component
- [X] The EventDispatcher Component
- [X] The ExpressionLanguage Component
- [X] The Filesystem Component
- [X] The Finder Component
- [X] The Form Component
- [X] The HttpFoundation Component
- [X] The HttpKernel Component
- [X] The Intl Component
- [ ] The Ldap Component (theoretically OK, but needs POC)
- [ ] The Lock Component (need to add a Memcache container)
- [X] The OptionsResolver Component
- [X] The PHPUnit Bridge
- [X] The Symfony Polyfill / APCu Component
- [X] The Symfony Polyfill / Ctype Component
- [X] The Symfony Polyfill / Iconv Component
- [X] The Symfony Polyfill / Intl Grapheme Component
- [X] The Symfony Polyfill / Intl ICU Component
- [X] The Symfony Polyfill / Mbstring Component
- [X] The Symfony Polyfill / PHP 5.4 Component
- [X] The Symfony Polyfill / PHP 5.5 Component
- [X] The Symfony Polyfill / PHP 5.6 Component
- [X] The Symfony Polyfill / PHP 7.0 Component
- [X] The Symfony Polyfill / PHP 7.1 Component
- [x] The Symfony Polyfill / PHP 7.2 Component
- [X] The Symfony Polyfill / PHP 7.3 Component
- [X] The Process Component
- [X] The PropertyAccess Component
- [X] The PropertyInfo Component
- [X] The PSR-7 Bridge
- [X] The Routing Component
- [ ] The Security Component (needs to POC Ldap first)
- [X] The Serializer Component
- [X] The Stopwatch Component
- [ ] The Templating Component (needs to have Varnish actually working)
- [X] The Translation Component
- [X] The Validator Component
- [X] The VarDumper Component
- [X] The Workflow Component
- [X] The Yaml Component

# Components for Symfony > 4.0

- [ ] The HttpClient Component (need to add a API Simulator container)
- [X] The Inflector Component
- [X] The Mailer Component
- [ ] The Mercure Component (needs, well, Mercure)
- [X] The Messenger Component
- [X] The Mime Component
- [X] The Symfony Polyfill / Intl IDN Component
- [X] The Symfony Polyfill / Intl MessageFormatter Component
- [X] The Symfony Polyfill / Intl Normalizer Component
- [X] The VarExporter Component
- [X] The WebLink Component


# Todo

Add services to make all the components above testable:
- [X] Get Symfony Server running
- [X] Add Database (MariaDB x1)
- [X] Add a Mailcatcher
- [X] Add Nginx
- [X] Add OpenLdap
- [X] Add RabbitMQ
- [X] Add Caches (Redis x 2 + Memcache x 1)
- [ ] Add Api Simulator
- [X] Add Varnish
- [ ] Add Mercure

## Todo later

It's always nice to have an ELK stack
- [ ] Elastic Search
- [ ] Kibana
- [ ] Logstash

# Ideas

## API
- API Simulator?
- Slim?

## Local CDN ideas
- DIY
- https://www.npmjs.com/package/local-cdn
- https://idiallo.com/blog/creating-your-own-cdn-with-nginx
